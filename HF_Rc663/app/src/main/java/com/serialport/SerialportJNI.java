package com.serialport;

public class SerialportJNI {
	
    //Scanner(1D & 2D)
    public native int WIrOpenSerialScan1D(String portName, int baudrate, int scanner);
    public native int WIrCloseSerialScan();
    public native int WIrOpenScanner1D();
    public native int WIrReadScanner1D(byte[] uReadData, byte[] uDataLen);
    public native int WIrStopScan1D();    
    public native int WIrOpenSerialScan2D(String portName, int baudrate);
    public native int WIrOpenScanner2D();
    public native int WIrReadScanner2D(byte[] uReadData);
    
    //HF(rc663)
    public native int Rc663_HFConnect(String portName, int baudrate);
    public native int Rc663_HFDisconnect();
    public native int Rc663_HFInit(int protocol);
    public native int Rc663_HFGetUid(byte[] uReadData, byte[] uDataLen);
    
    static {
		System.loadLibrary("com_serialport_SerialportJNI");
    }
	
}
