package com.jiangt.hf.rc663;

import java.util.ArrayList;
import java.util.List;

import com.serialport.AppContext;
import com.serialport.Module;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * 
 * @author synxu
 * 
 */
public class GridItem
{
    public static class Inner
    {

        private Inner()
        {
        }

        public static List<GridItem> getInternal(Module module)
        {
            List<GridItem> inner = new ArrayList<GridItem>();
            for (int i = 0; i < module.total; i++)
            {
            	Bitmap src = BitmapFactory.decodeResource(AppContext.current.getResources(), module.iconId[i]);
                inner.add(new GridItem(src, module.titleId[i], module.action[i]));
            }
            return inner;
        }
    }

    private Object action;

    private Bitmap icon;

    private String title;

    public GridItem()
    {
        super();
    }

    public GridItem(Bitmap icon, String title)
    {
        super();
        this.icon = icon;
        this.title = title;
    }

    public GridItem(int iconId, int titleId)
    {
        this(BitmapFactory.decodeResource(AppContext.current.getResources(), iconId), AppContext.current.getString(titleId));
    }

    public GridItem(Bitmap icon, int titleId, Object action)
    {
        this.icon = icon;
        title = AppContext.current.getString(titleId);
        setAction(action);
    }

    public Object getAction()
    {
        return action;
    }

    public Bitmap getIcon()
    {
        return icon;
    }

    public String getTitle()
    {
        return title;
    }

    public GridItem setAction(Object action)
    {
        this.action = action;
        return this;
    }

    public void setIcon(Bitmap icon)
    {
        this.icon = icon;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

}
