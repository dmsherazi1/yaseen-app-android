package com.jiangt.hf.rc663;

import java.util.Stack;
import com.serialport.AppContext;
import com.serialport.LogicFacade;
import com.serialport.ThreadPool;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/***
 * 类名称：SuperEcallActivity
 * 作者： JiangTao
 * 创建时间：2013-01-11
 * 类描述：公共Activity模板，所有界面Activity的基类
 */
public abstract class SuperEcallActivity extends Activity {

    protected static final String BG_COLOR_VALUE = "bg";

    private static final int EXIT = -1;

    protected static final Handler handler = new Handler();

    protected static Context skinContext;

    private static Stack<Activity> stack;

    protected static final String TAG = "ecall";

    private static final int WAIT = 1;

    private AlertDialog dialog;

    protected final DialogInterface.OnClickListener FINISH = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            onBackPressed();
        }
    };
    
    protected static DialogInterface.OnClickListener CANCEL_DFT = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
        }
    };

    protected static DialogInterface.OnClickListener OK_DFT = CANCEL_DFT;
    
    protected boolean flag;

    private int layoutResID;

    private String waitMessage;
    
    private SoundPool mSoundPool;
    private int musickey;
    private float audioMaxVolumn, audioCurrentVolumn, volumnRatio;
    
    //从JVM堆栈中结束Activity程序
    private void clear() {
        for (Activity a : stack) {
            if (!a.getClass().equals(GridHome.class)) {
                a.finish();
            }
        }
    }

    public void dismissDialog() {
        if (null != dialog && dialog.isShowing()) {
        	dialog.dismiss();
            dialog.cancel();
        }
    }

    protected void enterApp() {
        startActivity(new Intent(this, GridHome.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        clear();
    }
    
    //设置EditText输入框编辑模式不可用
  	protected void setEditTextEnable(EditText... mEditTexts){
  		for(EditText et : mEditTexts) {
  			et.setCursorVisible(false);
  			et.setFocusable(false);
  			et.setFocusableInTouchMode(false);
  		}
  	}

    //退出应用程序
    protected void exit() {
        showDialog(EXIT);
    }

    @Override
    public Resources getResources() {
        if (null != skinContext && !(skinContext instanceof SuperEcallActivity)) {
            return skinContext.getResources();
        }
        return super.getResources();
    }

    protected String getText(TextView et) {
        return et.getText().toString().trim();
    }

    protected abstract void initComp();

    /**
     * 
     * 方法名称：isEmpty
     * 作者：JiangTao
     * 方法描述：判断view包含文字是否为空,如果默认提示不为空,予以提示
     * 输入参数：@param view
     * 输入参数：@param defNote
     * 输入参数：@return
     * 返回类型：boolean：
     * 备注：
     */
    protected boolean isFilled(TextView view, String defNote) {
        if (!"".equals(getText(view))) {
            return true;
        }
        if (null != defNote && !"".equals(defNote.trim())) {
            showToast(defNote);
        }
        view.requestFocus();
        return false;
    }

    public boolean isPortal() {
        return Configuration.ORIENTATION_PORTRAIT == getResources()
            .getConfiguration().orientation;
    }

    protected void killPackage() {
        if (!LogicFacade.killPackage())
        {
            for (int i = SuperEcallActivity.stack.size() - 1; i > -1; i--)
            {
                SuperEcallActivity.stack.get(i).finish();
            }
        }
//        releaseWakeLock();
        android.os.Process.killProcess(android.os.Process.myPid());
        AppContext.setIns(null);
        skinContext = null;
    }

    @Override
    public void onBackPressed()
    {
    	dismissDialog();
        if (1 < LogicFacade.activities())
        {
            finish();
    		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
        else
        {
            exit();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppContext.current = this;
        if (null != savedInstanceState) {
            AppContext.setIns((AppContext) savedInstanceState.getParcelable("data"));
        }
        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //为音效做准备，创建声音池的对象
  		mSoundPool = new SoundPool(3, am.getStreamVolume(AudioManager.STREAM_SYSTEM), 100);
  		//创建声音资源的ID
  		musickey = mSoundPool.load(this, R.raw.sound1, 1);
  		
  		audioMaxVolumn = am.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        audioCurrentVolumn = am.getStreamVolume(AudioManager.STREAM_SYSTEM);
//        volumnRatio = audioCurrentVolumn/audioMaxVolumn;
        volumnRatio = 0.45f;
    }

    @Override
    protected Dialog onCreateDialog(int id)
    {
        switch (id)
        {
            case WAIT:
            {
                dialog = new ProgressDialog(this);
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
	                	@Override
	                	public void onCancel(DialogInterface arg0) {
	                		// TODO Auto-generated method stub
	                	}
                    });
                dialog.setMessage(waitMessage == null ? getString(R.string.waiting) : waitMessage);
                dialog.setCancelable(true);
                dialog.setIcon(android.R.drawable.ic_dialog_alert);
                return dialog;
            }
            case EXIT:
            {
                return new AlertDialog.Builder(this).setTitle(R.string.note)
                    .setMessage(R.string.sure_exit).setPositiveButton(
                        R.string.sure, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog,
                                int which)
                            {
                                killPackage();
                            }
                        }).setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog,
                                int which)
                            {
                                flag = false;
                                dismissDialog(EXIT);
                            }
                        }).setCancelable(true).setIcon(R.drawable.ic_launcher)
                    .create();
            }
            default:
            {
                break;
            }
        }
        return null;
    }

    protected void onDialogCancel()
    {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.general, menu);
        return true;
    }

    /**
     * (non-Javadoc)
     * 方法名称：onDestroy
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：
     * 返回类型：
     * 
     * @see android.app.Activity#onDestroy()
     *      备注：
     */
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        SuperEcallActivity.stack.remove(this);
    }

    /**
     * (non-Javadoc)
     * 方法名称：onOptionsItemSelected
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param item
     * 输入参数：@return
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     *      备注：
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.about:
            {
                showAbout();
                break;
            }
            case R.id.help:
            {
                showHelp();
                break;
            }
            case R.id.exit:
            {
                showDialog(EXIT);
                break;
            }
            default:
            {
                break;
            }
        }
        return false;
    }

    void showHelp()
    {
        if (null != AppContext.getIns().helpTxt)
        {
            showDialog(R.string.help, AppContext.getIns().helpTxt, OK_DFT, null);
        }
    }

    protected void showAbout()
    {
        showDialog(R.string.about, getString(R.string.aboutText), OK_DFT, null);
    }

    /**
     * (non-Javadoc)
     * 方法名称：onResume
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：
     * 返回类型：
     * 
     * @see android.app.Activity#onResume()
     *      备注：
     */
    @Override
    protected void onResume()
    {
        super.onResume();
        AppContext.current = this;
        pushCurrent();
    }

    /**
     * (non-Javadoc)
     * 方法名称：onSaveInstanceState
     * 作者：jiangtao
     * 方法描述：低内存时恢复数据
     * 输入参数：@param outState
     * 返回类型：
     * 
     * @see android.app.Activity#onSaveInstanceState(android.os.Bundle)
     *      备注：
     */
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        // 按下home键
        if (0 == LogicFacade.activities())
        {
            outState.putParcelable("data", AppContext.getIns());
        }
        super.onSaveInstanceState(outState);
    }

    /**
     * 方法名称：pushCurrent
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：
     * 返回类型：void：
     * 备注：
     */
    private void pushCurrent() {
        if (null == SuperEcallActivity.stack)
        {
            SuperEcallActivity.stack = new Stack<Activity>();
        }
        if (!SuperEcallActivity.stack.contains(this))
        {
            SuperEcallActivity.stack.push(this);
        }
    }
    
    protected void sendRequest(Runnable requestWorker) {
        if (null != requestWorker) {
            ThreadPool.submit(requestWorker);
        }
    }
    
    /**
	 * 播放音频资源
	 */
    protected void playSoundRes(){
		//监听资源加载完毕后开始播放音效  如果不是临时使用这个音频，则无需监听res是否加载完成
//		mSoundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
//			@Override
//			public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
				// TODO Auto-generated method stub
				mSoundPool.play(musickey, volumnRatio, volumnRatio, 1, 0, 1);  
//				try {
//					Thread.sleep(500);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		});
//		stopPlaySound();
	}
    
    /**
     * 停止播放
     */
    protected void stopPlaySound() {
    	if(null != mSoundPool) {
    		mSoundPool.release();
    	}
	}

    /**
     * 设置背景,立即生效
     * @param bg
     */
    protected void setBackground() {
        getWindow().setBackgroundDrawableResource(R.drawable.bg);
    }

    /**
     * 设置系统控件的颜色
     * @param view
     */
    protected void setCompColor(View view) {
        if (view instanceof RadioButton) {
            ((RadioButton) view).setTextColor(getResources().getColor(R.color.textColor));
        } else if (view instanceof Button) {
            ((Button) view).setTextColor(getResources().getColor(R.color.black));
        } else if (view instanceof EditText) {
//            ((EditText) view).setTextColor(getResources().getColor(R.color.editColor));
//            view.setBackgroundResource(R.drawable.edit_selector);
        } else if (view instanceof TextView) {
//            ((TextView) view).setTextColor(getResources().getColor(R.color.textColor));
        } else if (view instanceof ViewGroup) {
            setCompColor((ViewGroup) view);
        }
    }

    /**
     * 方法名称：setCompColor
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param root
     * 返回类型：void：
     * 备注：
     */
    private void setCompColor(ViewGroup root)
    {
        for (int i = 0; i < root.getChildCount(); i++)
        {
            View view = root.getChildAt(i);
            setCompColor(view);
        }
    }
    
    @Override
    public void setContentView(int layoutResID) {
        this.layoutResID = layoutResID;
        setContentView(LayoutInflater.from(this).inflate(layoutResID, null));
        setCompColor((ViewGroup) findViewById(R.id.Layout001));
        setBackground();
        setTitle();
    }

    /**
     * 初始化界面标题以及布局
     * @param layoutId{@link #setContentView(int)}
     */
    public void setCustomTileContentView(int layoutId) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(layoutId);
    }

    /**
     * 方法名称：setTitle
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：
     * 返回类型：void：
     * 备注：
     */
    private void setTitle()
    {
        String item = getIntent().getStringExtra("title");
        setTitle(null == item ? getString(R.string.title) : item);
    }

    /**
     * (non-Javadoc)
     * 方法名称：setTitle
     * 作者：jiangtao
     * 方法描述：设置自定义标题
     * 输入参数：@param title
     * 返回类型：
     * 
     * @see android.app.Activity#setTitle(java.lang.CharSequence)
     *      备注：
     */
    @Override
    public void setTitle(CharSequence title)
    {
        super.setTitle(title);
        TextView tv = (TextView) findViewById(R.id.title);
        findViewById(R.id.ct).setBackgroundResource(R.drawable.pageheader);
        tv.setTextColor(Color.WHITE);
        tv.setText(title);
    }

    /**
     * 
     * 方法名称：setValue
     * 作者：jiangtao
     * 方法描述：在非UI线程也可以执行ui操作
     * 输入参数：@param view
     * 输入参数：@param content
     * 返回类型：void：
     * 备注：
     */
    public void setValue(final TextView view, final CharSequence content)
    {
        runOnUiThread(new Runnable()
        {
            public void run()
            {
                if (view instanceof EditText)
                {
                    view.setText(null);
                    view.append(content == null ? "" : content);
                }
                else
                {
                    view.setText(content);
                }
//                setCompColor(view);
            }
        });
    }

    /**
     * 方法名称：setValue
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param view
     * 输入参数：@param key
     * 输入参数：@param dftValue
     * 返回类型：void：
     * 备注：
     */
    protected void setValue(TextView view, String key, String dftValue)
    {
        String value = LogicFacade.fromSharedPreferences(key, dftValue);
        view.setText(value);
    }

    /**
     * 方法名称：showDialog
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param resId
     * 输入参数：@param positive
     * 输入参数：@param negative
     * 返回类型：void：
     * 备注：
     */
    protected void showDialog(int resId, OnClickListener positive,
        OnClickListener negative)
    {
        showDialog(getString(resId), positive, negative);
    }

    /**
     * 方法名称：showDialog
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param titleId
     * 输入参数：@param msg
     * 输入参数：@param suc
     * 输入参数：@return
     * 返回类型：Dialog：
     * 备注：
     */
    public Dialog showDialog(int titleId, String msg, boolean suc)
    {
        return showDialog(titleId, msg, FINISH, suc ? null : CANCEL_DFT);
    }

    /**
     * 方法名称：showDialog
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param msg
     * 输入参数：@param suc
     * 输入参数：@return
     * 返回类型：Dialog：
     * 备注：
     */
    public Dialog showDialog(String msg, boolean suc)
    {
        return showDialog(suc ? R.string.note : R.string.error, msg, suc);
    }

    /**
     * 方法名称：showDialog
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param titleId
     * 输入参数：@param msg
     * 输入参数：@param positive
     * 输入参数：@param negative
     * 输入参数：@return
     * 返回类型：Dialog：
     * 备注：
     */
    public Dialog showDialog(final int titleId, final String msg,
        final DialogInterface.OnClickListener positive,
        final DialogInterface.OnClickListener negative)
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                dialog = new AlertDialog.Builder(AppContext.getIns().current)
                        .setMessage(msg).setPositiveButton(
                            null == positive ? "" : getString(R.string.sure),
                            positive).setNegativeButton(
                            null == negative ? "" : getString(R.string.cancel),
                            negative).setIcon(R.drawable.ic_launcher)
                        .setTitle(titleId).show();
            }
        });
        return dialog;
    }
    
    /**
     * 方法名称：showDialog
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param msg
     * 输入参数：@return
     * 返回类型：Dialog：
     * 备注：
     */
    public Dialog showDialog(final String msg)
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                dialog =
                    new AlertDialog.Builder(SuperEcallActivity.this)
                        .setMessage(msg).show();
            }
        });
        return dialog;
    }

    /**
     * 方法名称：showDialog
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param msg
     * 输入参数：@param positive
     * 输入参数：@param negative
     * 输入参数：@return
     * 返回类型：Dialog：
     * 备注：
     */
    public Dialog showDialog(String msg,
        DialogInterface.OnClickListener positive,
        DialogInterface.OnClickListener negative)
    {
        return showDialog(R.string.note, msg, positive, negative);
    }

    /**
     * 方法名称：showProgressDialog
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param resId
     * 返回类型：void：
     * 备注：
     */
    protected void showProgressDialog(int resId)
    {
        showProgressDialog(getString(resId));
    }
    
    /**
     * 方法名称：showProgressDialog
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param resId
     * 返回类型：void：
     * 备注：
     */
    protected void showProgressDialog(int resId, boolean flag)
    {
        showProgressDialog(getString(resId), flag);
    }

    /**
     * 方法名称：showProgressDialog
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param msg
     * 返回类型：void：
     * 备注：
     */
    public void showProgressDialog(String msg)
    {
        waitMessage = (null == msg || msg.trim().equals("") ? null : msg);
        runOnUiThread(new Runnable()
        {
            public void run()
            {
            	dialog = ProgressDialog.show(AppContext.current, getString(R.string.note), waitMessage);
            	dialog.setCancelable(true);
            }
        });
    }
    
    /**
     * 方法名称：showProgressDialog
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param msg
     * 返回类型：void：
     * 备注：
     */
    public void showProgressDialog(String msg, final boolean flag)
    {
        waitMessage = (null == msg || msg.trim().equals("") ? null : msg);
        runOnUiThread(new Runnable()
        {
            public void run()
            {
            	dialog = ProgressDialog.show(AppContext.current, getString(R.string.note), waitMessage);
            	dialog.setCancelable(flag);
            }
        });
    }

    /**
     * 方法名称：showSingleChoiceDialog
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param title
     * 输入参数：@param items
     * 输入参数：@param onItemClick
     * 返回类型：void：
     * 备注：
     */
    public void showSingleChoiceDialog(final String title,
        final String[] items, final DialogInterface.OnClickListener onItemClick)
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                new AlertDialog.Builder(SuperEcallActivity.this)
                    .setSingleChoiceItems(items, 0, onItemClick)
                    .setPositiveButton(R.string.sure, onItemClick)
                    .setNegativeButton(R.string.cancel, null).setIcon(
                        R.drawable.ic_launcher).setTitle(
                        null == title ? getString(R.string.note) : title)
                    .show();
            }
        });
    }

    /**
     * 方法名称：showToast
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param resId
     * 返回类型：void：
     * 备注：
     */
    protected void showToast(int resId)
    {
        showToast(getString(resId));
    }

    /**
     * 方法名称：showToast
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param msg
     * 返回类型：void：
     * 备注：
     */
    public void showToast(final String msg)
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(SuperEcallActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

}