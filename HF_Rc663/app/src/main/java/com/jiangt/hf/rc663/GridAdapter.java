package com.jiangt.hf.rc663;

import java.util.List;

import com.jiangt.hf.rc663.GridHome.ViewHolder;
import com.serialport.AppContext;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridAdapter extends BaseAdapter
{

    private List<GridItem> icons;

    public GridAdapter(List<GridItem> paras)
    {
        this.icons = paras;
    }

    @Override
    public int getCount()
    {
        return null == icons ? 0 : icons.size();
    }

    @Override
    public Object getItem(int position)
    {
        return icons == null ? null : icons.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder vh = null;
        if (null == convertView)
        {
            vh = new ViewHolder();
            convertView = AppContext.current.getLayoutInflater().inflate(R.layout.grid_item, null);
            vh.icon = (ImageView) convertView.findViewById(R.id.icon);
            vh.title = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(vh);
        }
        else
        {
            vh = (ViewHolder) convertView.getTag();
        }
        // vh.icon.setImageBitmap(LogicFacade.createThumbnail(
        // AppContext.current, icon.get(position).getIcon(), 42, 42));
        // vh.icon.setImageDrawable(icon.get(position).getIcon());
        vh.icon.setImageBitmap(icons.get(position).getIcon());
        // vh.icon.setImageDrawable(LogicFacade.clipBitmap(
        // AppContext.current, ((BitmapDrawable) icon.get(position)
        // .getIcon()).getBitmap(), 1, 0));
        // vh.title.setText(icons.get(position).getTitle());
        AppContext.current.setValue(vh.title, icons.get(position).getTitle());
        return convertView;
    }
}
