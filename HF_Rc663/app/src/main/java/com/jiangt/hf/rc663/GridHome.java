package com.jiangt.hf.rc663;

import java.util.ArrayList;
import java.util.List;
import com.serialport.AppContext;
import com.serialport.Module;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class GridHome extends SuperEcallActivity
{
    public static class ViewHolder
    {
        public ImageView icon;
        public TextView title;
    }

    protected List<GridItem> items;
    protected Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCustomTileContentView(R.layout.home);
        initComp();
    }

    @Override
    protected void initComp()
    {
        final GridView gv = (GridView) findViewById(R.id.gridlist);
        setGridAdapter(gv);
        gv.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                int position, long id)
            {
                GridItem gi = (GridItem) parent.getItemAtPosition(position);
                Object act = gi.getAction();
                Intent intent = new Intent();
                
                Log.i("action", gi.getTitle());
                Log.i("action", gi.getAction().toString());
                
                if (act instanceof Class<?>)
                {
                    intent = new Intent(GridHome.this, (Class<?>) act);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivityForResult(intent.putExtra("title", gi.getTitle()).putExtra("idx", (position + 1) + ""), 0);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                }
                // 外部应用
                else if (act instanceof Intent)
                {
                    startActivity((Intent) act);
                }
                else
                {
                    if (Integer.valueOf(act.toString()) == R.string.about)
                    {
                        showAbout();
                    }
                    else
                    {
                        showHelp();
                    }
                }
            }
        });
    }

    protected void setGridAdapter(GridView gv)
    {
        items = GridItem.Inner.getInternal(Module.desk);
        gv.setAdapter(new GridAdapter(items));
    }

    /**
     * 方法名称：createAppShortcut
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：@param packagename
     * 输入参数：@return
     * 返回类型：GridItem：
     * 备注：
     */
    public GridItem createAppShortcut(String packagename) {
        PackageManager pm = AppContext.current.getPackageManager();
        // Intent intent = pm.getLaunchIntentForPackage(packagename);
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // intent.addCategory(Intent.CATEGORY_LAUNCHER);
        PackageInfo ap = null;
        try {
            ap = pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
        } catch (NameNotFoundException e) {
            return null;
        }
        String name = ap.activities[0].loadLabel(pm).toString();
        Drawable icon = ap.activities[0].loadIcon(pm);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ComponentName cn = new ComponentName(ap.packageName, ap.activities[0].name);
        intent.setComponent(cn);
        GridItem gi = new GridItem(((BitmapDrawable) icon).getBitmap(), name);
        gi.setAction(intent);
        return gi;
    }

    /**
     * (non-Javadoc)
     * 方法名称：onDestroy
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：
     * 返回类型：
     * 
     * @see com.syn.ecall.ui.SuperEcallActivity#onDestroy()
     *      备注：
     */
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        recycle();
    }

    /**
     * 方法名称：recycle
     * 作者：jiangtao
     * 方法描述：
     * 输入参数：
     * 返回类型：void：
     * 备注：
     */
    private void recycle()
    {
        if (null != items && 0 != items.size())
        {
            for (GridItem gi : items)
            {
                if (null != gi.getIcon() && !gi.getIcon().isRecycled())
                {
                    Log.i(TAG, gi.getTitle() + "'s cycled");
                    gi.getIcon().recycle();
                }
            }
        }
    }

}
