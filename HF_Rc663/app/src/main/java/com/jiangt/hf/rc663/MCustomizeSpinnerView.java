package com.jiangt.hf.rc663;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * 类名称：MySpinnerButton.java
 * 类描述：自定义Spinner下拉列表控件
 * 作者：jiangtao
 * 创建时间：2013-04-02
 */
public class MCustomizeSpinnerView extends Button {

	private Context context;
	private String[] mDropdownItem;
	
	public MCustomizeSpinnerView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		setOnClickListener(new MySpinnerButtonOnClickListener());
	}

	public MCustomizeSpinnerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		setOnClickListener(new MySpinnerButtonOnClickListener());
	}

	public MCustomizeSpinnerView(Context context) {
		super(context);
		this.context = context;
		setOnClickListener(new MySpinnerButtonOnClickListener());
	}
	
	public void setmDropdownItem(String[] _mDropdownItem) {
		mDropdownItem = _mDropdownItem;
	}

	/**
	 * MySpinnerButton的点击事件
	 */
	class MySpinnerButtonOnClickListener implements View.OnClickListener{
		@Override
		public void onClick(View v) {
			final MySpinnerDropDownItems mSpinnerDropDrownItems = new MySpinnerDropDownItems(context);
            if (!mSpinnerDropDrownItems.isShowing()) { 
            	mSpinnerDropDrownItems.showAsDropDown(MCustomizeSpinnerView.this);
            } 
		}
	}
	
	/**
	 * MySpinnerButton的下拉列表
	 * @author jiangtao 
	 */
	class MySpinnerDropDownItems extends PopupWindow {
		private Context context;
		private LinearLayout mLayout;  // 下拉列表的布局
		private ListView mListView;    // 下拉列表控件
		
		public MySpinnerDropDownItems(Context context){
			super(context);
			
			this.context = context;
			// 下拉列表的布局
			mLayout = new LinearLayout(context);
			mLayout.setOrientation(LinearLayout.VERTICAL);
			// 下拉列表控件
			mListView = new ListView(context);
			mListView.setLayoutParams(new LayoutParams(MCustomizeSpinnerView.this.getWidth(), LayoutParams.WRAP_CONTENT));
			mListView.setCacheColorHint(Color.TRANSPARENT);
			// 为listView设置适配器
			mListView.setAdapter(new MyAdapter(context, R.layout.spinner_dropdown_item, mDropdownItem));
			// 设置listView的点击事件
			mListView.setOnItemClickListener(new MyListViewOnItemClickedListener());
			// 把下拉列表添加到layout中。
			mLayout.addView(mListView);
			
			setWidth(LayoutParams.WRAP_CONTENT);
			setHeight(LayoutParams.WRAP_CONTENT);
			setContentView(mLayout);
			setFocusable(true);
			
			mLayout.setFocusableInTouchMode(true);
		}
		
	    public class MyAdapter extends BaseAdapter {  
	      
	        private Context context;  
	        private int mResource;  
	        private String[] mFrom;  
	        private LayoutInflater mLayoutInflater;  
	          
	        public MyAdapter(Context context, int resource, String[] from){  
	            this.context = context;  
	            this.mResource = resource;  
	            this.mFrom = from;  
	            this.mLayoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);  
	        }  
	          
	        public int getCount() {  
	              
	            return mFrom.length;  
	        }  
	      
	        public Object getItem(int position) {  
	              
	            return mFrom[position];  
	        }  
	      
	        public long getItemId(int position) {  
	              
	            return position;  
	        }  
	      
	        @Override
	        public View getView(int position, View contentView, ViewGroup parent) {  
	            contentView = this.mLayoutInflater.inflate(this.mResource, parent, false);    
	            // 设置contentView的内容和样式，这里重点是设置contentView中文字的大小  
                TextView textView = (TextView) contentView.findViewById(R.id.spinner_dropdown_item_textview);  
                textView.setText(mFrom[position].toString());  
	            return contentView;  
	        }

	    } 
	    
	    /**
	     * listView的点击事件
	     */
	    class MyListViewOnItemClickedListener implements AdapterView.OnItemClickListener{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				TextView mTextView = (TextView) view.findViewById(R.id.spinner_dropdown_item_textview);
				String content = mTextView.getText().toString();
				MCustomizeSpinnerView.this.setText(content);
				MySpinnerDropDownItems.this.dismiss();
			}
	    }
	    
	}
}
