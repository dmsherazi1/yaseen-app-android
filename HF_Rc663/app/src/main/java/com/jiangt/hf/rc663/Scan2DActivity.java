package com.jiangt.hf.rc663;

import com.serialport.AppContext;
import com.serialport.Utilities;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

/**
 * 类名称：Scan1DActivity
 * 作者： jiangtao
 * 创建时间：2013-01-23
 * 类描述： 二维码扫描的Activity
 */
public class Scan2DActivity extends SuperEcallActivity implements OnClickListener {

	private static final String TAG = "Scan2DActivity";
	
	private TextView mScanReadBtn;
	private TextView mScanReadEditdetail;
	
	private static final int HANDLER_REFRESH = 0x99;
	private static final int HANDLER_OPEN_ACTIVITY = 0x97;
	
	private static int m_series = -1;
	private byte[] GetRFPower = new byte[2048];
	private int uDataLen;
	
	private Scan2DReadThread scan2DReadThread;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.scanning_main2);
		initComp();
	}
	
	@Override
	protected void initComp() {
		// TODO Auto-generated method stub
		setTitle(getString(R.string.Scan2D));
		mScanReadBtn = (TextView) findViewById(R.id.mScan2DReadBtn);
		mScanReadEditdetail = (TextView) findViewById(R.id.mScan2D_editdetail);
		mScanReadBtn.setOnClickListener(this);
		
		sendRequest(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				showProgressDialog(R.string.waiting_openModule, false);
				openSerialConn();
			}
		});
	}
	
	protected void openSerialConn() {
		m_series = AppContext.getJni().WIrOpenSerialScan2D(AppContext.SERIALPORTPATH_TTYSAC1, AppContext.SERIALPORTSPEED_B115200);
		
		Message message = new Message();
		message.what = HANDLER_OPEN_ACTIVITY;
		message.obj = m_series;
		message.setTarget(mhandler);
		mhandler.sendMessage(message);
	}
	
	protected void startScan2D() {
		if(AppContext.getJni().WIrOpenScanner2D() == 1){
			mScanReadEditdetail.setText("");
			scan2DReadThread = new Scan2DReadThread();
			scan2DReadThread.start();
		} else {
			Log.i("接收数据：", "写命令帧出现错误，打开串口失败");
		}
	}
	
	protected class Scan2DReadThread extends Thread {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			uDataLen = AppContext.getJni().WIrReadScanner2D(GetRFPower);
			if(uDataLen > 0){
				StringBuffer mRFBuffer = new StringBuffer();
				mRFBuffer.append(Utilities.Hex2DenaryStr(GetRFPower, uDataLen));
				
				Message message = new Message();
				message.what = HANDLER_REFRESH;
				message.obj = mRFBuffer;
				mhandler.sendMessage(message);
			} 
		}
	}
	
	Handler mhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			switch(msg.what) {
				case HANDLER_REFRESH:
					playSoundRes();
					mScanReadEditdetail.setText(msg.obj.toString());
					mScanReadEditdetail.invalidate();
					if(null != scan2DReadThread && !scan2DReadThread.isInterrupted()){
						scan2DReadThread.interrupt();
						scan2DReadThread = null;
					}
					break;
				case HANDLER_OPEN_ACTIVITY:
					if((Integer) msg.obj != 1) {
						dismissDialog();
						showDialog(R.string.error_device_open, FINISH, null);
					} else {
						dismissDialog();
						showToast(R.string.module_open_success);
					}
					break;
			}
		}
	};
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		AppContext.getJni().WIrCloseSerialScan();
		super.onBackPressed();
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		startScan2D();               
		//这里如果没安装模组的话，会进入死循环，所以先屏蔽
//		showDialog(R.string.error_device_open, CANCEL_DFT, null);   
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub		
		if((keyCode == KeyEvent.KEYCODE_HANDLE_SHORTCUT || keyCode == KeyEvent.KEYCODE_HANDLE_SHORTCUT1) 
				&& event.getRepeatCount() == 0) {
			if(null != scan2DReadThread && !scan2DReadThread.isInterrupted()) {
				return false;
			}
			startScan2D(); 
		} else if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(null != scan2DReadThread && !scan2DReadThread.isInterrupted()) {
				scan2DReadThread.interrupt();
				scan2DReadThread = null;
			}
			AppContext.getJni().WIrCloseSerialScan();
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return super.onKeyUp(keyCode, event);
	}
	
}