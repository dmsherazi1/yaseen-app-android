#ifndef SERIAL_H_
#define SERIAL_H_


int WSlOpenPort(int* fd, const char* PortName, int baudrate, char databits, char stopbits, char parity);

int WSlClosePort(int fd);

int WSlWritePort(int fd, unsigned char* Buf, int len);

int WSlReadPort(int fd, unsigned char* DatBuf, int nMax);

int WSlIsOpen(int fd);

void WSlClearComm(int fd);

#endif /* SERIAL_H_ */
