#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <android/log.h>
#include <GPIO.h>
#include <Serial.h>
#include <Rc663.h>

static const char *TAG="Rc663";
#define LOGI(fmt, args...) __android_log_print(ANDROID_LOG_INFO,  TAG, fmt, ##args)
#define LOGD(fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, TAG, fmt, ##args)
#define LOGE(fmt, args...) __android_log_print(ANDROID_LOG_ERROR, TAG, fmt, ##args)

#define GPIO_DRIVER_DEV_PATH    "/dev/GPM1"

static int m_series = -1;
static int m_protocol = -1;
int  Rc663_HFConnect(const char* PortName, int Baudrate)
{
	if (WSlOpenPort(&m_series, PortName, Baudrate, 8, 1, 'n'))
	{
		unsigned char uPower;

		LOGD("m_series: %d \n", m_series);

		{	//模块上电、拉高uart
			int gpioFd;
			gpioFd = gpioInit(GPIO_DRIVER_DEV_PATH);
			LOGI("gpioFd = %d", gpioFd);
			if(gpioFd < 0) return -1;
			char led = 0x01;
			gpioWrite(gpioFd, &led, 5);     //第三个参数对应kernel/drivers/uart_gpio_select/uart_gpio_select.c
			gpioExit(gpioFd);
		}

		Sleep(2000);	//设置2秒时间上电延时

		return 1;
	}
	return 0;
}

int  Rc663_HFDisconnect(void)
{
	{	//模块下电、拉低uart
		int gpioFd;
		gpioFd = gpioInit(GPIO_DRIVER_DEV_PATH);
		LOGI("gpioFd = %d", gpioFd);
		if(gpioFd < 0) return -1;
		char led = 0x01;
		gpioWrite(gpioFd, &led, 4);     //第三个参数对应kernel/drivers/uart_gpio_select/uart_gpio_select.c
		gpioExit(gpioFd);
	}

	Sleep(30);

	WSlClosePort(m_series);
	m_series = -1;
	return 1;
}

int  Rc663_HFInit(int protocol)
{
	m_protocol = protocol;
	LOGI("Rc663_HFInit...");

	if (protocol == 0)   	// ISO14443 A协议
	{
		Configure(m_protocol);  	//配置模块TM1， TM2， TM3, TM4时钟频率, 配置寄存器 SIZE = 255 and Water-level
		Flush_FiFo();       		//终止所有命令并清空寄存器
		WriteFifo();        		//将TX，RX的协议号写入寄存器
	}

	if(protocol == 1) 		// ISO14443 B协议
	{
		Configure(m_protocol);
	}

	if (protocol == 2)		// ISO15693协议
	{
		Configure(m_protocol);     //配置模块TM1， TM2， TM3, TM4时钟频率, 配置寄存器 SIZE = 255 and Water-level
//		ApplySet();
//		ActiveCard();
	}
	return 1;
}

void  Request()
{
	LOGI("Request...");
	unsigned char Command[35] = {0x31, 0xC0, 0x32, 0x0B, 0x10, 0x08, 0x11, 0x94, 0x15, 0x00, 0x16, 0x00, 0x06, 0x08, 0x36, 0x90, 0x2e, 0x0f, 0x00, 0x00, 0x02, 0xB0, 0x06, 0x7F, 0x07, 0x7F, 0x05, 0x26, 0x00, 0x07, 0x08, 0x18, 0x09, 0x42};
	unsigned char Readbuf[64] = {0};
	int len = 0;

	WSlClearComm(m_series);

	len = WSlWritePort(m_series, Command, sizeof(Command) - 1);
	printLog(Command, sizeof(Command) - 1);

	Sleep(10);

	len = WSlReadPort(m_series, Readbuf, sizeof(Readbuf));
	printLog(Readbuf, sizeof(Readbuf));

	if (GetFIFO(IRQ1) != 0x60)
	{
		return;
	}
}

int  Rc663_HFGetUid(unsigned char* uReadData, unsigned char* uDataLen)
{
	int i;
	LOGI("Rc663_HFGetUid...  m_protocol=%d", m_protocol);

	if (m_protocol == 0)
	{
		unsigned char Command[23] = {0x2e, 0x08, 0x0c, 0x00, 0x00, 0x00, 0x02, 0xB0, 0x06, 0x7F, 0x07, 0x7F, 0x05, 0x93, 0x05, 0x20, 0x00, 0x07, 0x08, 0x18, 0x09, 0x42};
		unsigned char Readbuf[64] = {0};
		int len = 0;
		unsigned char Status = 0;

//		Request();
		StartRC663();       		//启动RC663
		ConfigCRC();        		//配置CEC校验算法
		Request();

		WSlClearComm(m_series);

		len = WSlWritePort(m_series, Command, sizeof(Command) - 1);
		LOGI("WSlWritePort len=%d", len);
		printLog(Command, sizeof(Command) - 1);

		if (len == 0)
		{
			return 0;
		}

		Sleep(10);

		memset(Readbuf, 0, sizeof(Readbuf));
		len = WSlReadPort(m_series, Readbuf, sizeof(Readbuf));

		LOGI("WSlReadPort len=%d", len);
		printLog(Readbuf, sizeof(Readbuf));

		if (len == 0)
		{
			return 0;
		}
		Status = GetFIFO(IRQ1);

		LOGI("WSlReadPort Status=%d", Status);

		SetFIFO(IRQ0_EN, 0x00);
		SetFIFO(IRQ0_EN, 0x00);
		GetFIFO(IRQ0);
		GetFIFO(IRQ1);
		int FIFOLen = GetFIFO(FIFO_LEN);
		LOGI("11111111111111 WSlReadPort FIFOLen=%d", FIFOLen);
		if (FIFOLen == 0x00 || FIFOLen == 0xFF)
		{
			return 0;
		}

		unsigned char temp[100] = {0};

		LOGI("WSlReadPort FIFOLen=%d", FIFOLen);

		if(FIFOLen > 0) {
			for (i = 1; i < FIFOLen; i++)
			{
				temp[FIFOLen - i] = GetFIFO(FIFO_DATA);
			}
			printLog(temp, FIFOLen);

			uDataLen[0] = FIFOLen - 1;
			memcpy(uReadData, temp + 1, uDataLen[0]);
			printLog(uReadData, uDataLen[0]);

			return 1;
		}
	}
	else if (m_protocol == 1)
	{
		SendREQB();
		int len = GetFIFO(FIFO_LEN);

		LOGI("ISO14443B: len1=%d", len);

		unsigned char data[12];
		for (i = 1; i < len; i++)
		{
			data[i-1] = GetFIFO(FIFO_DATA);

		}
		SetFIFO(COMMAND, 0x00);
		SetFIFO(FIFO_CTRL, 0xB0);
		SetFIFO(IRQ0, 0x7f);
		SetFIFO(IRQ1, 0x7f);
		SetFIFO(FIFO_DATA, 0x1d);
		SetFIFO(FIFO_DATA, 0x00);
		SetFIFO(FIFO_DATA, 0x00);
		SetFIFO(FIFO_DATA, 0x00);
		SetFIFO(FIFO_DATA, 0x00);
		SetFIFO(FIFO_DATA, 0x00);
		SetFIFO(FIFO_DATA, 0x08);
		SetFIFO(FIFO_DATA, 0x01);
		SetFIFO(FIFO_DATA, 0x08);
		SetFIFO(COMMAND, 0x07);

		len = GetFIFO(FIFO_LEN);
		LOGI("ISO14443B: len2=%d", len);

		if(len == 0)
		{
			len = GetFIFO(OPERATION_ERROR);
		}
		SetFIFO(COMMAND, 0x00);
		SetFIFO(FIFO_CTRL, 0xB0);
		SetFIFO(IRQ0, 0x7f);
		SetFIFO(IRQ1, 0x7f);
		SetFIFO(FIFO_DATA, 0x00);
		SetFIFO(FIFO_DATA, 0x36);
		SetFIFO(FIFO_DATA, 0x00);
		SetFIFO(FIFO_DATA, 0x00);
		SetFIFO(FIFO_DATA, 0x08);
		SetFIFO(COMMAND, 0x07);

		len = GetFIFO(FIFO_LEN);
		LOGI("ISO14443B: len3=%d", len);

		if(len == 0)
		{
			len = GetFIFO(OPERATION_ERROR);
		}

		if (len != 0x0A)
		{
			return 0;
		}

		for (i = 1; i < len; i++)
		{
			data[i-1] = GetFIFO(FIFO_DATA);

		}

		unsigned char temp[4] = {0};
//		sprintf(temp, "%02X%02X%02X%02X", data[1], data[2], data[3], data[4]);
		for(i = 0; i < 4; i++){
			temp[i] = data[i + 1];
		}
		printLog(temp, sizeof(temp));

		uDataLen[0] = 4;
		memcpy(uReadData, temp, uDataLen[0]);
		printLog(uReadData, uDataLen[0]);

		return 1;
	}
	else if (m_protocol == 2)
	{
		ApplySet();
		ActiveCard();

		unsigned char Command[16] = {0x84, 0x85, 0x85, 0x85, 0x85, 0x85, 0x85, 0x85, 0x85, 0x85, 0x85, 0x85, 0x86, 0x87, 0x84};
		unsigned char Readbuf[64] = {0};
		int len = 0;

		WSlClearComm(m_series);

		len = WSlWritePort(m_series, Command, 15);
		printLog(Command, sizeof(Command));

		if (len == 0)
		{
			return 0;
		}

		Sleep(10);

		memset(Readbuf, 0, sizeof(Readbuf));
		len = WSlReadPort(m_series, Readbuf, sizeof(Readbuf));
		printLog(Readbuf, sizeof(Readbuf));

		if (len == 0)
		{
			return 0;
		}

		if (Readbuf[0] != 0x0A)
		{
			return 0;
		}

		unsigned char temp[10] = {0};
		for(i = 0; i < 10; i++){
//			Readbuf[1], Readbuf[2], Readbuf[3], Readbuf[4], Readbuf[5], Readbuf[6], Readbuf[7], Readbuf[8], Readbuf[9], Readbuf[10]
			temp[i] = Readbuf[i + 1];
		}

		printLog(temp, sizeof(temp));

		uDataLen[0] = 10;
		memcpy(uReadData, temp, uDataLen[0]);
		printLog(uReadData, uDataLen[0]);

		return 1;
	}
	return 0;
}

void  SendREQB()
{
	GetFIFO(DRV_MODE);
	SetFIFO(DRV_MODE, 0x87);
	SetFIFO(DRV_MODE, 0x8F);
	SetFIFO(TXWAIT_CTRL, 0xc1);
	SetFIFO(TXWAIT_LO, 0x0b);
	SetFIFO(COMMAND, 0x00);
	SetFIFO(FIFO_CTRL, 0xB0);
	SetFIFO(IRQ0, 0x7F);
	SetFIFO(IRQ1, 0x7F);
	SetFIFO(FIFO_DATA, 0x05);
	SetFIFO(FIFO_DATA, 0x00);
	SetFIFO(FIFO_DATA, 0x00);
    SetFIFO(COMMAND, 0x07);
};

int SetFIFO(unsigned char FIFOAddr, unsigned char SettingMode)
{
	unsigned char Command[2] = {0x00};
	unsigned char ReadBuf[2] = {0x00};
    Command[0] = FIFOAddr;
    Command[1] = SettingMode;
	int len = 0;

	LOGI("SetFIFO...");

	WSlClearComm(m_series);

	len = WSlWritePort(m_series, Command, sizeof(Command));
	printLog(Command, sizeof(Command));

	if(len == 0)
	{
		return 0;
	}

	Sleep(10);

	len = 0;
    len = WSlReadPort(m_series, ReadBuf, sizeof(ReadBuf));
    printLog(ReadBuf, sizeof(ReadBuf));

    if (len == 0)
	{
		return 0;
	}

	return 1;
}

void Configure(int Protocol)
{
	LOGI("Configure...");

	if (Protocol == 2)
	{
		unsigned char Command[49] = {0x0F, 0x98, 0x14, 0x92, 0x19, 0x20, 0x1A, 0x03, 0x1b, 0xff, 0x1e, 0x00, 0x02, 0x90, 0x03, 0xfe, 0x0c, 0x80, 0x28, 0x80, 0x29, 0x00, 0x2a, 0x01, 0x2b, 0x05, 0x34, 0x00, 0x38, 0x12, 0x00, 0x00, 0x02, 0xb0, 0x06, 0x7F, 0x07, 0x7F, 0x05, 0x0a, 0x05, 0x0a, 0x08, 0x10, 0x09, 0x40, 0x00, 0x0d};
		unsigned char Readbuf[64] = {0};
		int len = 0;

		WSlClearComm(m_series);

		len = WSlWritePort(m_series, Command, sizeof(Command) - 1);
		printLog(Command, sizeof(Command) - 1);

		Sleep(10);

		len = WSlReadPort(m_series, Readbuf, sizeof(Readbuf));
		printLog(Readbuf, sizeof(Readbuf));
	}

	if (Protocol == 1)
	{
		unsigned char Command[81] = {0x0F, 0x98, 0x37, 0xFF, 0x14, 0x92, 0x19, 0x20, 0x1a, 0x03, 0x1b, 0xff, 0x1e, 0x00, 0x02, 0x90, 0x03, 0xfe, 0x0c, 0x80, 0x28, 0x8F, 0x29, 0xCC, 0x2a, 0x01, 0x2b, 0x05, 0x34, 0x00, 0x38, 0x12, 0x00, 0x00, 0x02, 0xb0, 0x06, 0x7F, 0x07, 0x7F, 0x05, 0x04, 0x05, 0x04, 0x08, 0x10, 0x09, 0x40, 0x00, 0x0d, 0x08, 0x00, 0x09, 0x00, 0x02, 0xb0, 0x2c, 0x7b, 0x2d, 0x7b, 0x2e, 0x08, 0x2f, 0x0a, 0x30, 0x00, 0x31, 0x01, 0x33, 0x05, 0x34, 0xb2, 0x35, 0x34, 0x37, 0x3f, 0x38, 0x12, 0x39, 0x0a};
		unsigned char Readbuf[64] = {0};
		int len = 0;

		WSlClearComm(m_series);

		len = WSlWritePort(m_series, Command, sizeof(Command) - 1);
		printLog(Command, sizeof(Command) - 1);

		Sleep(10);

		len = WSlReadPort(m_series, Readbuf, sizeof(Readbuf));
		printLog(Readbuf, sizeof(Readbuf));
	}

    if(Protocol == 0)
	{
		if(!SetFIFO(T0_CTRL, 0x98))   //T0_CTRL=0x0F
		{
			return;
		}
		if(!SetFIFO(T1_CTRL, 0x92))
		{
			return;
		}
		if(!SetFIFO(T2_CTRL, 0x20))
		{
			return;
		}
		if(!SetFIFO(T2_RELOADHI, 0x03))
		{
			return;
		}
		if(!SetFIFO(T2_RELOADLO, 0xFF))
		{
			return;
		}
		if(!SetFIFO(T3_CTRL, 0x00))
		{
			return;
		}
		if(!SetFIFO(FIFO_CTRL, 0x90))
		{
			return;
		}
		if(!SetFIFO(WATER_LEVEL, 0xFE))
		{
			return;
		}
		if(!SetFIFO(RXBIT_CTRL, 0x80))
		{
			return;
		}
		if(!SetFIFO(DRV_MODE, 0x80))
		{
			return;
		}
		if(!SetFIFO(TX_AMP, 0x00))
		{
			return;
		}
		if(!SetFIFO(DRV_CON, 0x01))
		{
			return;
		}
		if(!SetFIFO(TXL, 0x05))
		{
			return;
		}
		if(!SetFIFO(RX_SOFD, 0x00))
		{
			return;
		}
		if(!SetFIFO(RCV, 0x12))
		{
			return;
		}
	}
}

void  Flush_FiFo()
{
	LOGI("Flush_FiFo...");
	if(!SetFIFO(COMMAND, 0x00))   // 停止运行所有的动作
	{
		return;
	}
	if(!SetFIFO(FIFO_CTRL, 0xB0))   //设置FIFO
	{
		return;
	}
	if(!SetFIFO(IRQ0, 0x7F))   //设置中断0
	{
		return;
	}
	if(!SetFIFO(IRQ1, 0x7F))   //设置中断1
	{
		return;
	}
}

void  WriteFifo()
{
	LOGI("WriteFifo...");
	if(!SetFIFO(FIFO_DATA, 0x00))
	{
		return;
	}
	if(!SetFIFO(FIFO_DATA, 0x00))
	{
		return;
	}
	if(!SetFIFO(IRQ0_EN, 0x10))   //设置中断0
	{
		return;
	}
	if(!SetFIFO(IRQ1_EN, 0x40))   //设置中断1
	{
		return;
	}
}

void  StartRC663()
{
	LOGI("StartRC663...");
	if(!SetFIFO(COMMAND, 0x0D))
	{
		return;
	}
	if(GetFIFO(IRQ1) != 0x60)
	{
		return;
	}
	if(!SetFIFO(IRQ0_EN, 0x00))
	{
		return;
	}
	if(!SetFIFO(IRQ1_EN, 0x00))   //设置中断0
	{
		return;
	}
	if(!SetFIFO(FIFO_CTRL, 0xb0))   //设置中断1
	{
		return;
	}
}

void  ConfigCRC()
{
	LOGI("ConfigCRC...");
	if(!SetFIFO(TXCRC_PRESET, 0x18))
	{
		return;
	}
	if(!SetFIFO(RXCRC_PRESET, 0x18))
	{
		return;
	}
	if(!SetFIFO(TXDATA_NUM, 0x08))   //设置中断0
	{
		return;
	}
	if(!SetFIFO(TXMODE_WID, 0x20))
	{
		return;
	}
	if(!SetFIFO(TXSYM10_BURSTLEN, 0x00))
	{
		return;
	}
	if(!SetFIFO(FRAME_CON, 0xCF))
	{
		return;
	}
	if(!SetFIFO(RX_CTRL, 0x04))
	{
		return;
	}
	if(!SetFIFO(RX_THRESHOLD, 0x32))
	{
		return;
	}
	if(!SetFIFO(RX_ANA, 0x00))
	{
		return;
	}
	if(!SetFIFO(RX_WAIT, 0x90))
	{
		return;
	}
	if(!SetFIFO(TXWAIT_CTRL, 0xC0))
	{
		return;
	}
	if(!SetFIFO(TXWAIT_LO, 0x0b))
	{
		return;
	}
	if(!SetFIFO(T0_RELOADHI, 0x08))
	{
		return;
	}
	if(!SetFIFO(T0_RELOADLO, 0xD8))
	{
		return;
	}
	if(!SetFIFO(T1_RELOADHI, 0x00))
	{
		return;
	}
	if(!SetFIFO(T1_RELOADLO, 0x00))
	{
		return;
	}
	if(!SetFIFO(DRV_MODE, 0x81))
	{
		return;
	}
	if(!SetFIFO(STATUS, 0x00))
	{
		return;
	}
	if(!SetFIFO(DRV_MODE, 0x89))
	{
		return;
	}
}

unsigned char  GetFIFO(unsigned char FIFOAddr)
{
	unsigned char Command[2] = {0x00};
	unsigned char ReadBuf[2] = {0x00};
	int len = 0;

	LOGE("GetFIFO...");

	FIFOAddr = FIFOAddr|0x80;
	Command[0] = FIFOAddr;

	WSlClearComm(m_series);

	len = WSlWritePort(m_series, Command, sizeof(Command) - 1);
	printLog(Command, sizeof(Command) - 1);

	if(len == 0)
	{
		return 0xFF;
	}

	Sleep(30);

	len = 0;
	len = WSlReadPort(m_series, ReadBuf, sizeof(ReadBuf));
	printLog(ReadBuf, sizeof(ReadBuf));

	if (len == 0)
	{
		return 0xFF;
	}
	return ReadBuf[0];
}

void  ApplySet()
{
	LOGI("ApplySet...");
	unsigned char Command[47] = {0x08, 0x00, 0x09, 0x00, 0x02, 0xB0, 0x2c, 0x7b, 0x2d, 0x7b, 0x2e, 0x08, 0x2f, 0x00, 0x30, 0x00, 0x31, 0x00, 0x33, 0x0f, 0x35, 0x02, 0x37, 0x4e, 0x39, 0x04, 0x36, 0x8c, 0x31, 0xc0, 0x32, 0x00, 0x10, 0x18, 0x11, 0x86, 0x15, 0x00, 0x16, 0x00, 0x29, 0x0a, 0x28, 0x81, 0x0b, 0x00};
	unsigned char Readbuf[64] = {0};
	int len = 0;

	WSlClearComm(m_series);

	len = WSlWritePort(m_series, Command, sizeof(Command) - 1);
	printLog(Command, sizeof(Command) - 1);

	Sleep(10);

	len = WSlReadPort(m_series, Readbuf, sizeof(Readbuf));
	printLog(Readbuf, sizeof(Readbuf));
}


void  ActiveCard()
{
	LOGI("ActiveCard...");
	unsigned char Command[44] = {0x28, 0x89, 0x10, 0x24, 0x11, 0xeb, 0x15, 0x00, 0x16, 0x00, 0x00, 0x00, 0x02, 0xb0, 0x06, 0x7f, 0x07, 0x7f, 0x05, 0x36, 0x05, 0x01, 0x05, 0x00, 0x05, 0x00, 0x00, 0x07, 0x88, 0x08, 0x18, 0x89, 0x09, 0x42, 0x87, 0x88, 0x08, 0x00, 0x89, 0x09, 0x00, 0x86, 0x87};
	unsigned char Readbuf[64] = {0};
	int len = 0;

	WSlClearComm(m_series);

	len = WSlWritePort(m_series, Command, sizeof(Command) - 1);
	printLog(Command, sizeof(Command) - 1);

	Sleep(10);

	len = WSlReadPort(m_series, Readbuf, sizeof(Readbuf));
	printLog(Readbuf, sizeof(Readbuf));
}
