#ifndef GPIO_H_
#define GPIO_H_

int   gpioInit (const char* devname);
void  gpioExit (int fd);
void  gpioSetDir (int fd, int cmd, int bit);

int gpioWrite (int fd, int cmd, int bit);
int gpioRead (int fd, int cmd, int bit);

#endif /* R1000_H_ */
