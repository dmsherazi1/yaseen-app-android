#ifndef Scanner_H_
#define Scanner_H_

#include <Serial.h>

int WIrOpenSerialScan(const char* PortName, int Baudrate, int scanner);

int WIrCloseSerialScan(void);

int WIrOpenScanner1D();

int WIrReadScanner1D(unsigned char* uReadData, unsigned char* uDataLen);

int WIrStopScan1D ();

int WIrOpenScanner2D();

int WIrReadScanner2D(unsigned char* uReadData);

#endif /* Scanner_H_ */
