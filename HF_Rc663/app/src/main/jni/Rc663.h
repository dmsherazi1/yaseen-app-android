#ifndef Rc663_H_
#define Rc663_H_

#define COMMAND          0x00
#define HOST_CTRL        0x01
#define FIFO_CTRL        0x02
#define WATER_LEVEL      0x03
#define FIFO_LEN         0x04
#define FIFO_DATA        0x05
#define IRQ0             0x06
#define IRQ1             0x07
#define IRQ0_EN          0x08
#define IRQ1_EN          0x09
#define OPERATION_ERROR  0x0a
#define STATUS           0x0B
#define RXBIT_CTRL       0x0c
#define RX_COLL          0x0d
#define T_CTRLL          0x0e
#define T0_CTRL          0x0F
#define T0_RELOADHI      0x10
#define T0_RELOADLO      0x11
#define T0_COUNTVALHI    0x12
#define T0_COUNTVALLO    0x13
#define T1_CTRL          0x14
#define T1_RELOADHI      0x15
#define T1_RELOADLO      0x16
#define T1_COUNTVALHI    0x17
#define T1_COUNTVALLO    0x18
#define T2_CTRL          0x19
#define T2_RELOADHI      0x1A
#define T2_RELOADLO      0x1B
#define T2_COUNTVALHI    0x1C
#define T2_COUNTVALLO    0x1D
#define T3_CTRL          0x1E
#define T3_RELOADHI      0x1F
#define T3_RELOADLO      0x20
#define T3_COUNTVALHI    0x21
#define T3_COUNTVALLO    0x22
#define T4_CTRL          0x23
#define T4_RELOADHI      0x24
#define T4_RELOADLO      0x25
#define T4_COUNTVALHI    0x26
#define T4_COUNTVALLO    0x27
#define DRV_MODE         0x28
#define TX_AMP           0x29
#define DRV_CON          0x2a
#define TXL              0x2b
#define TXCRC_PRESET     0x2c
#define RXCRC_PRESET     0x2d
#define TXDATA_NUM       0x2E
#define TXMODE_WID       0x2F
#define TXSYM10_BURSTLEN 0x30
#define TXWAIT_CTRL      0x31
#define TXWAIT_LO        0x32
#define FRAME_CON        0x33
#define RX_SOFD          0x34
#define RX_CTRL          0x35
#define RX_WAIT          0x36
#define RX_THRESHOLD     0x37
#define RCV              0x38
#define RX_ANA           0x39

#include <Serial.h>

int  Rc663_HFConnect(const char* PortName, int Baudrate);

int  Rc663_HFDisconnect(void);

int  Rc663_HFInit(int protocol);

int  Rc663_HFGetUid(unsigned char* uReadData, unsigned char* uDataLen);


void 	Flush_FiFo();       //终止所有命令并清空寄存器
void 	WriteFifo();        //将TX，RX的协议号写入寄存器
void 	StartRC663();       //启动RC663
void 	ConfigCRC();        //配置CEC校验算法

void 	Request();          //发送协议请求
void 	ReadFIFO();         //读取寄存器
int 	GetUid(int Protocol);           //获取UID+
void 	SendREQB();
void 	ApplySet();         //确认设置
void 	ActiveCard();       //激活卡

int 	SetFIFO(unsigned char FIFOAddr, unsigned char SettingMode);   //设置寄存器状态
unsigned char 	GetFIFO(unsigned char FIFOAddr);//获取寄存器状态

#endif /* Rc663_H_ */
