/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dostmuhammad.azkarOwazaif;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dostmuhammad.azkarOwazaif.R;

/**
 * A fragment representing a single step in a wizard. The fragment shows a dummy title indicating
 * the page number, along with some dummy text.
 * <p/>
 * <p>This class is used by the {@link Book} and {@link
 * YaseenActivity} samples.</p>
 */
public class YaseenPageFragment extends Fragment {
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";
    public static int[] pages = {

            R.drawable.p023,
            R.drawable.p022,
            R.drawable.p021,
            R.drawable.p020,
            R.drawable.p019,
            R.drawable.p018,
            R.drawable.p017,
            R.drawable.p016,
            R.drawable.p015,
            R.drawable.p014,
            R.drawable.p013,
            R.drawable.p012,
            R.drawable.p011,
            R.drawable.p010,
            R.drawable.p009,
            R.drawable.p008,
            R.drawable.p007,
            R.drawable.p006,
            R.drawable.p005,
            R.drawable.p004,
            R.drawable.p003,
            R.drawable.p002,
            R.drawable.p001,
            R.drawable.yaseen

    };

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;

    public YaseenPageFragment() {
    }

    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    public static YaseenPageFragment create(int pageNumber) {
        YaseenPageFragment fragment = new YaseenPageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout containing a title and body text.
        ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.fragment_screen_slide_page, container, false);

        // Set the title view to show the page number.
       /* ((TextView) rootView.findViewById(android.R.id.text1)).setText(
                getString(R.string.title_template_step, mPageNumber + 1)+"/23");*/
        /*((ImageView)rootView.findViewById(R.id.imView).setBackgroundResource(););*/
        ((ImageView) rootView.findViewById(android.R.id.text2)).setImageResource(pages[mPageNumber % YaseenActivity.NUM_PAGES]);
        return rootView;
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
}
